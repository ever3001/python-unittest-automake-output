# SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: MIT

import putao.unittest
import unittest

if __name__ == "__main__":
    runner = putao.unittest.TestRunner
    unittest.main(module=None, testRunner=runner)
